(ns neural-networks.math
  (:require [clojure.core.matrix :as m]))

(defn sigmoid [z] (/ 1.0 (unchecked-add-int (Math/exp (- z)))))

(defn layer [{:keys [num-neurons transfer-fn weights biases] :or {transfer-fn}}]
  {:pre [(integer? num-neurons) (ifn? transfer-fn) (= num-neurons (count weights) (count biases))]}
  {:num-neurons num-neurons
   })
