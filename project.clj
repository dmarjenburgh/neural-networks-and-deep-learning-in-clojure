(defproject neural-networks "0.0.1-SNAPSHOT"
  :dependencies [[net.mikera/core.matrix "0.62.0"]
                 [net.mikera/vectorz-clj "0.47.0"]
                 [net.mikera/imagez "0.12.0"]])
