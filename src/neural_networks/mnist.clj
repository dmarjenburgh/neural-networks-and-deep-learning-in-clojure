(ns neural-networks.mnist
  (:require [clojure.java.io :as io]
            [clojure.core.matrix :as m]
            [mikera.image.core :as im]
            [mikera.image.colours :as colours])
  (:import [java.io InputStream ByteArrayOutputStream]))

;; Loads the data from the mnist dataset

(defn- read-int [^InputStream is]
  (+ (bit-shift-left (.read is) 24) (bit-shift-left (.read is) 16) (bit-shift-left (.read is) 8) (.read is)))

(defn load-labels [f]
  (with-open [is (io/input-stream f)]
    (let [magic-number (read-int is)
          num-items (read-int is)
          os (ByteArrayOutputStream. num-items)
          _ (io/copy is os)
          items (m/matrix (.toByteArray os))]
      (when (not= num-items (m/ecount items))
        (println "WARNING - Item count doesn't match:" num-items "expected," (m/ecount items) "found"))
      items)))

(defn load-images [f]
  (with-open [is (io/input-stream f)]
    (let [magic-number (read-int is)
          num-images (read-int is)
          num-rows (read-int is)
          num-cols (read-int is)
          read-row (fn [is] (m/matrix (repeatedly num-cols #(.read is))))
          read-image (fn [is] (m/matrix (repeatedly num-rows (partial read-row is))))]
      (loop [i 0 out []]
        (if (< i num-images)
          (recur (inc i) (conj out (read-image is)))
          out)))))

(defn show-image [image & opts]
  (let [[m n] (m/shape image)
        new-image (im/new-image m n)
        pxs (im/get-pixels new-image)
        to-rgb (fn [in] (let [gr (- 255 in)] (colours/rgb-from-components gr gr gr)))]
    (im/set-pixels new-image (int-array (m/emap to-rgb (m/matrix :persistent-vector (m/reshape image [(* m n)])))))
    (apply im/show new-image opts)))

(defn load-data []
  (let [train-im-f "../data/train-images.idx3-ubyte"
        train-label-f "../data/train-labels.idx1-ubyte"
        test-im-f "../data/t10k-images.idx3-ubyte"
        test-label-f "../data/t10k-labels.idx1-ubyte"]
    {:training-images (load-images train-im-f)
     :training-labels (load-labels train-label-f)
     :test-images (load-images test-im-f)
     :test-labels (load-labels test-label-f)}))

(comment
  (m/set-current-implementation :vectorz)
  (load-labels "../data/train-labels.idx1-ubyte")
  (def data (load-data))
  )